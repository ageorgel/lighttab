package ro.georgelaron.lighttab;

import ro.georgelaron.R;

public class LightAnimation {
    public enum Animation {
        LEFT_IN(R.animator.slide_left_in), LEFT_OUT(R.animator.slide_left_out), RIGHT_IN(R.animator.slide_right_in);

        private int animation;

        private Animation(int animation) {
            this.animation = animation;
        }

        public int getAnimation() {
            return this.animation;
        }
    }

    public static int getComplementaryAnimation(Animation anim) {
        switch (anim) {
        case LEFT_IN:
            return R.animator.slide_right_out;
        default:
            return R.animator.stay_still;
        }
    }

}
